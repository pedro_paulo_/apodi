# README Animated Icons

Dentro de cada diretório desses há uma estrutura padrão de direótrios, sendo ela a seguinte:

-   DIRETÓRIO PAI (Nome do ícone animado)
    -   ANIMATIONS
    -   PREFAB
    -   SCRIPTS

## 1. Animations

Diretório contendo o **Controlador de Estados** juntamente com os **Estados** do ícone.
Todos os estados são nomeados:

-   **VeryLowLevel** (Nível muito baixo)
-   **LowLevel** (Nível baixo)
-   **NormalLevel** (Nível normal)
-   **HighLevel** (Nível alto)
-   **VeryHighLevel** (Nível muito alto)

Assim podendo repretar os cinco estados possíveis das animações.

## 2. Prefab

Diretório contendo o **Prefab** do ícone animado.

## 3. Scripts

Diretório contendo o **Script C Sharp** que controla as transações entre as animações dos ícones.
Os estados dos ícones são alterados via comando `Animator.Play(NameState)`. Não há **Transitions** entre os estados no Controlador de Estados do ícone.

### 3.1 Estrura dos Scripts

A estrutura dos scripts são identicas.

```csharp
public static class __Levels
{
	/*
	Esta classe estática serve para guardar os valores padrões e constantes dos níveis a serem considerados. Ou seja, por exemplo:

	A partir de que nível de temperatura é considerado normal?
	- Este valor deve ser guardado na constante NormalLevel

	Isso para todas outras constantes.
	*/

	public const float NormalLevel = 100f;
	...
}

public class __Control : MonoBehaviour
{
	// Valor do nível atual da grandeza (°C, ppm ...).
	// Esse valor deve vim da API.
	private float Current__Level;
	...

	void Change__ControlIconState()
	{
		/*
		Esse método é o responsável por alterar o estado do ícone a partir da variação do valor do nível atual da grandeza vindo da API.
		*/
		...
		string NameState;
		...
		else if (Current__Level >= __Levels.NormalLevel &&
				 Current__Level < __Levels.HighLevel)
		{
			NameState = "NormalLevel";
		}
		...
		Animator.Play(NameState);
		Previous__Level = Current__Level;
	}

	void TestingTransitions()
	{
		/*
		Esse método é apenas para testar se as transações estão acontecendo correntamente.
		Esse método deve ser substituído por um que atualize o valor do nível atual da grandeza a partir da API.
		*/

		if (Input.GetDown("1"))
		{
			Current__Level = 50f;
		}
		...
	}
}
```

## 4. Observações

1. Devida a composição dos ícones ser a junção de duas imagens independentes, ao incluí-los numa **Scene** talvez seja preciso fazer algumas alterações no objeto.
2. Para testar os ícones individualmente, há uma **Scene** específica para isso: `Assets > Scenes > AnimationsTest`.
