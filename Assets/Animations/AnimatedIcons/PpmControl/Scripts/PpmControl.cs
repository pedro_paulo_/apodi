﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PpmLevels
{
    public const float NormalLevel = 100f;
	
	public const float LowLevel = 75f;

	public const float HighLevel = 135f;
    public const float VeryHighLevel = 175f;
}

public class PpmControl : MonoBehaviour
{
    private float CurrentPpmLevel;
	private float PreviousPpmLevel;
	private Animator Animator;

	void Start()
    {
        Animator = GetComponent<Animator>();
    }

    void Update()
    {
        TestingTransitions();
		ChangePpmControlIconState();
    }

	void ChangePpmControlIconState()
    {
        if (PreviousPpmLevel != CurrentPpmLevel)
        {
            string NameState;
            if (CurrentPpmLevel < PpmLevels.LowLevel)
            {
                NameState = "VeryLowLevel";
            }
            else if (CurrentPpmLevel >= PpmLevels.LowLevel &&
                     CurrentPpmLevel < PpmLevels.NormalLevel)
            {
                NameState = "LowLevel";
            }
            else if (CurrentPpmLevel >= PpmLevels.NormalLevel &&
                     CurrentPpmLevel < PpmLevels.HighLevel)
            {
                NameState = "NormalLevel";
            }
            else if (CurrentPpmLevel >= PpmLevels.HighLevel &&
                     CurrentPpmLevel < PpmLevels.VeryHighLevel)
            {
                NameState = "HighLevel";
            }
            else
            {
                NameState = "VeryHighLevel";
            }
            Animator.Play(NameState);
            PreviousPpmLevel = CurrentPpmLevel;
        }
    }

	void TestingTransitions()
    {
        if (Input.GetKeyDown("1")) // VeryLowLevel
        {
            CurrentPpmLevel = 50;
        }
        else if (Input.GetKeyDown("2")) // LowLevel
        {
            CurrentPpmLevel = 86;
        }
        else if (Input.GetKeyDown("3")) // NormalLevel
        {
            CurrentPpmLevel = 102;
        }
        else if (Input.GetKeyDown("4")) // HighLevel
        {
            CurrentPpmLevel = 135;
        }
        else if (Input.GetKeyDown("5")) // VeryHighLevel
        {
            CurrentPpmLevel = 182;
        }
    }
}
