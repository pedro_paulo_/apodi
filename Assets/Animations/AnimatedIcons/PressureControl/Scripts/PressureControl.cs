﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PressureLeves
{
    public const float NormalLevel = 100f;

    public const float LowLevel = 75f;

    public const float HighLevel = 135f;
    public const float VeryHighLevel = 175f;
}

public class PressureControl : MonoBehaviour
{
    private float CurrentPressureLevel;
    private float PreviousPressureLevel;
    private Animator Animator;

    void Start()
    {
        Animator = GetComponent<Animator>();
    }

    void Update()
    {
        TestingTransitions();
        ChangePressureControlIconState();
    }

    void ChangePressureControlIconState()
    {
        if (PreviousPressureLevel != CurrentPressureLevel)
        {
            string NameState;
            if (CurrentPressureLevel < PressureLeves.LowLevel)
            {
                NameState = "VeryLowLevel";
            }
            else if (CurrentPressureLevel >= PressureLeves.LowLevel &&
                     CurrentPressureLevel < PressureLeves.NormalLevel)
            {
                NameState = "LowLevel";
            }
            else if (CurrentPressureLevel >= PressureLeves.NormalLevel &&
                     CurrentPressureLevel < PressureLeves.HighLevel)
            {
                NameState = "NormalLevel";
            }
            else if (CurrentPressureLevel >= PressureLeves.HighLevel &&
                     CurrentPressureLevel < PressureLeves.VeryHighLevel)
            {
                NameState = "HighLevel";
            }
            else
            {
                NameState = "VeryHighLevel";
            }
            Animator.Play(NameState);
            PreviousPressureLevel = CurrentPressureLevel;
        }
    }

    void TestingTransitions()
    {
        if (Input.GetKeyDown("1")) // VeryLowLevel
        {
            CurrentPressureLevel = 50;
        }
        else if (Input.GetKeyDown("2")) // LowLevel
        {
            CurrentPressureLevel = 86;
        }
        else if (Input.GetKeyDown("3")) // NormalLevel
        {
            CurrentPressureLevel = 102;
        }
        else if (Input.GetKeyDown("4")) // HighLevel
        {
            CurrentPressureLevel = 135;
        }
        else if (Input.GetKeyDown("5")) // VeryHighLevel
        {
            CurrentPressureLevel = 182;
        }
    }
}
