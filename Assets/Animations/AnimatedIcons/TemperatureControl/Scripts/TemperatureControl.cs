﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TemperatureLevels
{
	public const float NormalLevel = 100f;
	
	public const float LowLevel = 75f;

	public const float HighLevel = 135f;
    public const float VeryHighLevel = 175f;
}

public class TemperatureControl : MonoBehaviour
{
    private float CurrentTemperatureLevel;
	private float PreviousTemperatureLevel;
	private Animator Animator;
	
	void Start()
    {
        Animator = GetComponent<Animator>();
    }

    void Update()
    {
        TestingTransitions();
		ChangeTemperatureControlIconState();
    }

	void ChangeTemperatureControlIconState()
    {
        if (PreviousTemperatureLevel != CurrentTemperatureLevel)
        {
            string NameState;
            if (CurrentTemperatureLevel < TemperatureLevels.LowLevel)
            {
                NameState = "VeryLowLevel";
            }
            else if (CurrentTemperatureLevel >= TemperatureLevels.LowLevel &&
                     CurrentTemperatureLevel < TemperatureLevels.NormalLevel)
            {
                NameState = "LowLevel";
            }
            else if (CurrentTemperatureLevel >= TemperatureLevels.NormalLevel &&
                     CurrentTemperatureLevel < TemperatureLevels.HighLevel)
            {
                NameState = "NormalLevel";
            }
            else if (CurrentTemperatureLevel >= TemperatureLevels.HighLevel &&
                     CurrentTemperatureLevel < TemperatureLevels.VeryHighLevel)
            {
                NameState = "HighLevel";
            }
            else
            {
                NameState = "VeryHighLevel";
            }
            Animator.Play(NameState);
            PreviousTemperatureLevel = CurrentTemperatureLevel;
        }
    }

	void TestingTransitions()
    {
        if (Input.GetKeyDown("1")) // VeryLowLevel
        {
            CurrentTemperatureLevel = 50;
        }
        else if (Input.GetKeyDown("2")) // LowLevel
        {
            CurrentTemperatureLevel = 86;
        }
        else if (Input.GetKeyDown("3")) // NormalLevel
        {
            CurrentTemperatureLevel = 102;
        }
        else if (Input.GetKeyDown("4")) // HighLevel
        {
            CurrentTemperatureLevel = 135;
        }
        else if (Input.GetKeyDown("5")) // VeryHighLevel
        {
            CurrentTemperatureLevel = 182;
        }
    }
}
