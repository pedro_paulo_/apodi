﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ElectricCurrentLevels
{
    public const float NormalLevel = 100f;

    public const float LowLevel = 75f;

    public const float HighLevel = 135f;
    public const float VeryHighLevel = 175f;
}

public class ElectricCurrentControl : MonoBehaviour
{
    private float CurrentElectricCurrentLevel;
    private float PreviousElectricCurrentLevel;
    private Animator Animator;

    void Start()
    {
        Animator = GetComponent<Animator>();
    }

    void Update()
    {
        TestingTransitions();
        ChangeElectricCurrentControlIconState();
    }

    void ChangeElectricCurrentControlIconState()
    {
        if (PreviousElectricCurrentLevel != CurrentElectricCurrentLevel)
        {
            string NameState;
            if (CurrentElectricCurrentLevel < ElectricCurrentLevels.LowLevel)
            {
                NameState = "VeryLowLevel";
            }
            else if (CurrentElectricCurrentLevel >= ElectricCurrentLevels.LowLevel &&
                     CurrentElectricCurrentLevel < ElectricCurrentLevels.NormalLevel)
            {
                NameState = "LowLevel";
            }
            else if (CurrentElectricCurrentLevel >= ElectricCurrentLevels.NormalLevel &&
                     CurrentElectricCurrentLevel < ElectricCurrentLevels.HighLevel)
            {
                NameState = "NormalLevel";
            }
            else if (CurrentElectricCurrentLevel >= ElectricCurrentLevels.HighLevel &&
                     CurrentElectricCurrentLevel < ElectricCurrentLevels.VeryHighLevel)
            {
                NameState = "HighLevel";
            }
            else
            {
                NameState = "VeryHighLevel";
            }
            Animator.Play(NameState);
            PreviousElectricCurrentLevel = CurrentElectricCurrentLevel;
        }
    }

    void TestingTransitions()
    {
        if (Input.GetKeyDown("1")) // VeryLowLevel
        {
            CurrentElectricCurrentLevel = 50;
        }
        else if (Input.GetKeyDown("2")) // LowLevel
        {
            CurrentElectricCurrentLevel = 86;
        }
        else if (Input.GetKeyDown("3")) // NormalLevel
        {
            CurrentElectricCurrentLevel = 102;
        }
        else if (Input.GetKeyDown("4")) // HighLevel
        {
            CurrentElectricCurrentLevel = 135;
        }
        else if (Input.GetKeyDown("5")) // VeryHighLevel
        {
            CurrentElectricCurrentLevel = 182;
        }
    }
}
