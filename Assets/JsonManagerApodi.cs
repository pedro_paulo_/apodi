﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JsonManagerApodi : MonoBehaviour
{
    #region Test
    
    #endregion

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public static class JsonHelper
    {
        public static T[] FromJson<T>(string json)
        {
            Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
            return wrapper.Items;
        }

        public static string ToJson<T>(T[] array)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.Items = array;
            return JsonUtility.ToJson(wrapper);
        }

        public static string ToJson<T>(T[] array, bool prettyPrint)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.Items = array;
            return JsonUtility.ToJson(wrapper, prettyPrint);
        }

        [Serializable]
        private class Wrapper<T>
        {
            public T[] Items;
        }
    }

    [Serializable]
    public class Maquina
    {
        public dynamic id;
        public dynamic code;
        public dynamic name;
        public dynamic order;
        public dynamic parent_id;
        public dynamic [] subComponents;

        public Maquina(dynamic id, dynamic code, dynamic name, dynamic order, dynamic parent_id)
        {
            this.id = id;
            this.code = code;
            this.name = name;
            this.order = order;
            this.parent_id = parent_id;
            this.subComponents = subComponents;
        }
    }

    [Serializable]
    public class AlertedParameters
    {
        public dynamic id;
        public dynamic code;
        public dynamic name;
        public dynamic unit;
        public dynamic displayFormat;
        public dynamic [] values;
        public dynamic threshold_hh;
        public dynamic threshold_h;
        public dynamic threshold_ll;
        public dynamic threshold_l;
        public dynamic alerted;
        public dynamic lastUpdate;
        public dynamic componentId;

        public AlertedParameters(dynamic id, dynamic code, dynamic name, dynamic unit, dynamic displayFormat, dynamic[] values, dynamic threshold_hh, dynamic threshold_h, dynamic threshold_ll, dynamic threshold_l, dynamic alerted, dynamic lastUpdate, dynamic componentId)
        {
            this.id = id;
            this.code = code;
            this.name = name;
            this.unit = unit;
            this.displayFormat = displayFormat;
            this.values = values;
            this.threshold_hh = threshold_hh;
            this.threshold_h = threshold_h;
            this.threshold_ll = threshold_ll;
            this.threshold_l = threshold_l;
            this.alerted = alerted;
            this.lastUpdate = lastUpdate;
            this.componentId = componentId;
        }
    }

    [Serializable]
    public class Parametro
    {
        public dynamic id;
        public dynamic code;
        public dynamic name;
        public dynamic unit;
        public dynamic displayFormat;
        public dynamic threshold_hh;
        public dynamic threshold_h;
        public dynamic threshold_ll;
        public dynamic threshold_l;
        public dynamic alerted;
        public dynamic lastUpdate;
        public dynamic component_id;
        public dynamic order;
        public dynamic[] values;

        public Parametro() {

        }

        public Parametro(dynamic id, dynamic code, dynamic name, dynamic unit, dynamic displayFormat, dynamic threshold_hh, dynamic threshold_h, dynamic threshold_ll, dynamic threshold_l, dynamic alerted, dynamic lastUpdate, dynamic component_id, dynamic order)
        {
            this.id = id;
            this.code = code;
            this.name = name;
            this.unit = unit;
            this.displayFormat = displayFormat;
            this.threshold_hh = threshold_hh;
            this.threshold_h = threshold_h;
            this.threshold_ll = threshold_ll;
            this.threshold_l = threshold_l;
            this.alerted = alerted;
            this.lastUpdate = lastUpdate;
            this.component_id = component_id;
            this.order = order;
        }
    }

    
}
