﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using UnityEngine.SceneManagement;

public class ClientApodi : MonoBehaviour
{

    public Text message;
    public GameObject machine;
    public InputField textInputField;
    float targetTime = 60.0f;

    string CurrentText;

    ParameterManager pr;
    List<JsonManagerApodi.Parametro> listaParametros;
    List<JsonManagerApodi.AlertedParameters> listaAlertas;
    List<JsonManagerApodi.Maquina> listaMaquinas;

    public Text URLTEXT;
    GameObject controleQR;

    #region Elementos Fixos
    [SerializeField]
    private GameObject parametrosPrefab;

    [SerializeField]
    private GameObject paramList;

    #endregion

    #region URL's
    string urlRoot = "http://172.18.9.183:8080";
    string getURLbyQRCode = "/components/qrcode/";
    string postURL = "";
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        message.text = "Scan para iniciar";
        machine.SetActive(false);
        getURLbyQRCode = urlRoot + getURLbyQRCode;

        controleQR = GameObject.Find("QRCodeCameraManager");
    }

    public void UpdateUrl(string url)
    {
        urlRoot = "http://" + url;
        //Debug.Log(urlRoot);
    }

    public void GetParametrosByQRCode(string qr)
    {
        
        //Debug.Log(qr);
        message.text = "atualizando dados...";
        StartCoroutine(GetParametrosRequest(qr));
        if (machine.activeSelf == false) {
            machine.SetActive(true);   
        }

    }

    public void GetAlarmes()
    {
        message.text = "atualizando dados...";
        StartCoroutine(GetAlarmesRequest());
    }

    public void GetAllComponents()
    {
        message.text = "atualizando dados...";
        StartCoroutine(GetAllComponentsRequest());
    }

    // Update is called once per frame
    void Update()
    {
        targetTime -= Time.deltaTime;
        if (targetTime == 60.0f)
        {
            UpdateList();
        }
        else if (targetTime <= 0.0f)
        {
            targetTime = 60.0f;
        }

    }

    void UpdateList()
    {
        message.text = CurrentText;

        foreach (Transform child in paramList.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        if (listaParametros != null)
        {
            //qRCode.OnReset();
            for (int i = 0; i < listaParametros.Count; i++)
            {
                GameObject go = Instantiate(parametrosPrefab, paramList.transform);
                GameObject param = go.transform.Find("ParametroNome").gameObject;
                param.GetComponent<Text>().text = listaParametros[i].name;

                GameObject value = go.transform.Find("ParametroValor").gameObject;
                string stringValue = (string)listaParametros[i].values[0].ToString();
                value.GetComponent<Text>().text = float.Parse(stringValue, CultureInfo.InvariantCulture.NumberFormat).ToString("#.##");

                GameObject unit = go.transform.Find("Unidade").gameObject;
                unit.GetComponent<Text>().text = listaParametros[i].unit;

                var imageObject =  go.GetComponent<ParameterManager>();
                imageObject.unit = listaParametros[i].name;

            }
        }
    }

    void UpdateAlerts()
    {
        //Método de atualização dos alertas.
    }


    IEnumerator GetAlarmesRequest()
    {
        
        UnityWebRequest toHost = UnityWebRequest.Get(urlRoot + "/parameters/alertedParameters/public");

        yield return toHost.SendWebRequest();

        if (toHost.isNetworkError || toHost.isHttpError)
        {
            Debug.Log(toHost.error);
        }
        else
        {
            message.text = toHost.downloadHandler.text;

            listaAlertas = DeserializeAlertas(message.text);
            
        }
    }

    IEnumerator GetParametrosRequest(string qr)
    {
        //Debug.Log(getURLbyQRCode + qr + "/public");

        URLTEXT.text = getURLbyQRCode + qr + "/public";

        UnityWebRequest toHost = UnityWebRequest.Get(getURLbyQRCode + qr + "/public");

        yield return toHost.SendWebRequest();

        if (toHost.isNetworkError || toHost.isHttpError)
        {
            Debug.Log(toHost.error);
        }
        else
        {
            message.text = toHost.downloadHandler.text;

            listaParametros = DeserializeParametrosJson(message.text);

            UpdateList();
        }
    }

    IEnumerator GetAllComponentsRequest()
    {
        UnityWebRequest toHost = UnityWebRequest.Get(urlRoot + "/components/public");

        yield return toHost.SendWebRequest();

        if (toHost.isNetworkError || toHost.isHttpError)
        {
            Debug.Log(toHost.error);
        }
        else
        {
            message.text = toHost.downloadHandler.text;

            listaMaquinas = DeserializeAllComponents(message.text);
        }
    }



    public void OnButtonSendScore()
    {
        if (message.text == string.Empty)
        {
            textInputField.text = "Error: No high score to send. \nEnter a value in input field.";
        }
        else
        {
            textInputField.text = "Sending data...";
            StartCoroutine(SimplePostRequest(textInputField.text));
        }
    }

    IEnumerator SimplePostRequest(string dados)
    {
        List<IMultipartFormSection> Form = new List<IMultipartFormSection>();

        Form.Add(new MultipartFormDataSection("dadosKey", dados));

        UnityWebRequest postWebRequest = UnityWebRequest.Post(postURL, Form);

        yield return postWebRequest.SendWebRequest();

        if (postWebRequest.isNetworkError || postWebRequest.isHttpError)
        {
            Debug.LogError(postWebRequest.error);
        }
        else
        {
            message.text = postWebRequest.downloadHandler.text;
        }
    }

    #region Deserialize

    private List<JsonManagerApodi.Parametro> DeserializeParametrosJson(string JSON)
    {
        List<JsonManagerApodi.Parametro> parametros = new List<JsonManagerApodi.Parametro>();
        try
        {
            var jObject = JsonConvert.DeserializeObject<dynamic>(JSON);
            //Debug.Log("Objeto: " + jObject.ToString());
            CurrentText = jObject.name;
            //Debug.Log(CurrentText);

            foreach (var parametro in jObject.parameters)
            {
                JsonManagerApodi.Parametro newParameter = new JsonManagerApodi.Parametro(parametro.id, parametro.code, parametro.name, parametro.unit, parametro.displayFormat, parametro.threshold_hh, parametro.threshold_h, parametro.threshold_ll, parametro.threshold_l, parametro.alerted, parametro.lastUpdate, parametro.component_id, parametro.order);
                newParameter.values = parametro.values.ToObject<string[]>();
                
                parametros.Add(newParameter);

            }
            //return null;
            return parametros;
        }
        catch (Exception e)
        {
            Debug.Log("Erro de deserialização:" + e.Message.ToString());

            return null;
        }
    }

    private List<JsonManagerApodi.AlertedParameters> DeserializeAlertas(string JSON)
    {
        List<JsonManagerApodi.AlertedParameters> listaAvisos = new List<JsonManagerApodi.AlertedParameters>();
        try
        {
            var jObject = JsonConvert.DeserializeObject<dynamic>(JSON);
            //Debug.Log("Objeto: " + jObject.ToString());

            foreach (var alerta in jObject)
            {
                JsonManagerApodi.AlertedParameters newAviso = new JsonManagerApodi.AlertedParameters(alerta.id, alerta.code, alerta.name, alerta.unit, alerta.displayFormat, alerta.values, alerta.threshold_hh, alerta.threshold_h, alerta.threshhold_ll, alerta.threshold_l, alerta.alerted, alerta.lastUpdate, alerta.componentId);
                listaAvisos.Add(newAviso);
            }
            //return listaAvisos;
            return listaAvisos;
        }
        catch (Exception e)
        {
            Debug.Log("Erro de deserialização alertas:" + e.Message.ToString());

            return null;
        }
    }
    
    private List<JsonManagerApodi.Maquina> DeserializeAllComponents(string JSON)
    {
        List<JsonManagerApodi.Maquina> listaMaquinas = new List<JsonManagerApodi.Maquina>();
        try
        {
            var jObject = JsonConvert.DeserializeObject<dynamic>(JSON);
            //Debug.Log("Objeto: " + jObject.ToString());

            foreach (var maquina in jObject)
            {
                JsonManagerApodi.Maquina newMaquina = new JsonManagerApodi.Maquina(maquina.id, maquina.code, maquina.name, maquina.order, maquina.parent_id);
                foreach (var subcomponent in newMaquina.subComponents)
                {
                    //ARRUMAR PARA AMANHÃ
                    JsonManagerApodi.Maquina newSubMaquina = new JsonManagerApodi.Maquina(subcomponent.id, subcomponent.code, subcomponent.name, subcomponent.order, subcomponent.parent_id);
                    listaMaquinas.Add(newSubMaquina);
                }
                listaMaquinas.Add(newMaquina);
            }
            return listaMaquinas;
        }
        catch (Exception e)
        {
            Debug.Log("Erro de deserialização dos componentes:" + e.Message.ToString());

            return null;
        }
    }
    #endregion
}

