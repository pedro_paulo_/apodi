﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApodiScreenManager : MonoBehaviour
{
    [SerializeField]
    private GameObject login;

    [SerializeField]
    private GameObject mainScreen;

    [SerializeField]
    private GameObject warningScreen;


    [SerializeField]
    private GameObject mostrarButton;
    [SerializeField]
    private GameObject esconderButton;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Logar()
    {
        login.SetActive(false);
        mainScreen.SetActive(true);
    }

    public void MostrarAlarmes()
    {
        if (warningScreen.activeSelf == true)
        {
            warningScreen.SetActive(false);
        }
        else if(warningScreen.activeSelf == false)
        {
            warningScreen.SetActive(true);
        }
    }

    public void MostrarEsconderParametros()
    {
        if (mostrarButton.activeSelf)
        {
            mostrarButton.SetActive(false);
            esconderButton.SetActive(true);
        }
        else
        {
            mostrarButton.SetActive(true);
            esconderButton.SetActive(false);
        }
    }
}
