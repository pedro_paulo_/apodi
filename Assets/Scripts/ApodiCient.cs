﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class ApodiClient : MonoBehaviour
{
    private SubEstacaoData[] allSubestationData;

    public Text message;
    public InputField textInputField;

    public GameObject subestacaoPrefab;

    string urlRoot = "";
    

    readonly string getURL = "172.18.9.189:8080/oauth/token";
    readonly string postURL = "";

    // Start is called before the first frame update
    void Start()
    {
        message.text = "aaaaa";
    }

    public void OnButtonGetScore()
    {

        message.text = "atualizando dados...";

        StartCoroutine(SimpleGetRequest());

    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator SimpleGetRequest()
    {


        UnityWebRequest toHost = UnityWebRequest.Get(getURL);

        yield return toHost.SendWebRequest();

        if (toHost.isNetworkError || toHost.isHttpError)
        {
            Debug.Log(toHost.error);
        }
        else
        {
            //List<SubEstacaoData> stationList = new List<SubEstacaoData>();
            //APAGAR ISSO DPS
            message.text = toHost.downloadHandler.text;

            deserializeJSON(message.text);
        }
    }

    public void OnButtonSendScore()
    {
        if (message.text == string.Empty)
        {
            textInputField.text = "Error: No high score to send. \nEnter a value in input field.";
        }
        else
        {
            textInputField.text = "Sending data...";
            StartCoroutine(SimplePostRequest(textInputField.text));
        }
    }

    IEnumerator SimplePostRequest(string dados)
    {
        List<IMultipartFormSection> Form = new List<IMultipartFormSection>();

        Form.Add(new MultipartFormDataSection("dadosKey", dados));

        UnityWebRequest postWebRequest = UnityWebRequest.Post(postURL, Form);

        yield return postWebRequest.SendWebRequest();

        if (postWebRequest.isNetworkError || postWebRequest.isHttpError)
        {
            Debug.LogError(postWebRequest.error);
        }
        else
        {
            message.text = postWebRequest.downloadHandler.text;
        }
    }

    private void deserializeJSON(string JSON)
    {
        //COLOCAR EM OUTRO MÉTODO
        SubEstacaoData substation = new SubEstacaoData();
        List<SubEstacaoData> substations = new List<SubEstacaoData>();

        try
        {
            var jObject = JsonConvert.DeserializeObject<dynamic>(JSON);
            Debug.Log("Objeto: " + jObject.ToString());

            //COLOCAR ME OUTRO MÉTODO
            foreach (var station in jObject)
            {
                substation.id = station.id;
                substation.name = station.name;
                substation.sicode = station.sicode;
                substation.sectional = station.sectional;
                substation.regional = station.regional;
                Debug.Log(substation.id);
                substations.Add(substation);
            }


        }
        catch (Exception e)
        {
            Debug.Log("Erro de deserialização:" + e.Message.ToString());

        }
    }

    class SubEstacaoData
    {
        public string id { get; set; }
        public string name { get; set; }
        public string sicode { get; set; }
        public string sectional { get; set; }
        public string regional { get; set; }
        public float lat { get; set; }
        public float longe { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }

    }

}
