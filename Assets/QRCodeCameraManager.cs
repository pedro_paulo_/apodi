﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ZXing;
using ZXing.QrCode;

public class QRCodeCameraManager : MonoBehaviour
{
    WebCamTexture webcam;
    bool isActive = false;
    public Text tex;
    GameObject client;
    private WebCamTexture camTexture;
    private Rect screenRect;
    bool startQR = false;

    string initialTex;


    // Start is called before the first frame update
    void Start()
    {
        screenRect = new Rect(0, 0, Screen.width, Screen.height);
        camTexture = new WebCamTexture();
        camTexture.requestedHeight = Screen.height;
        camTexture.requestedWidth = Screen.width;

        client = GameObject.Find("Screens");
        initialTex = tex.text;
    }

    // Update is called once per frame
    void Update()
    {
        if (startQR)
        {
            
        }
    }

    void MakeClientRequest()
    {
        if (tex.text != null)
        {
            client.GetComponent<ClientApodi>().GetParametrosByQRCode(tex.text);
        }
    }

    #region Controle de QRcode
    public void StartScan()
    {
        // if (camTexture != null)
        //{
            if (!startQR)
            {
                startQR = true;
                camTexture.Play();
            }
            else
            {
                startQR = false;
                camTexture.Play();
            }
        //}
    }


    void OnGUI()
    {
        if (startQR)
        {
            // drawing the camera on screen
            GUI.DrawTexture(screenRect, camTexture, ScaleMode.ScaleToFit);
            // do the reading — you might want to attempt to read less often than you draw on the screen for performance sake
            try
            {
                IBarcodeReader barcodeReader = new BarcodeReader();
                // decode the current frame
                var result = barcodeReader.Decode(camTexture.GetPixels32(), camTexture.width, camTexture.height);
                if (result != null)
                {
                    tex.text = result.Text;
                    Debug.Log("DECODED TEXT FROM QR: " + result.Text);
                    startQR = false;

                    if (tex.text != "Maquina")
                    {
                        MakeClientRequest();
                    }
                }
            }
            catch (Exception ex) { Debug.LogWarning(ex.Message); }
        }
    }
    #endregion

    #region Controle de Camera
    public void StartStopCamera()
    {
        if (isActive == true)
        {
            webcam.Stop();
            isActive = false;
        }
        else
        {
            webcam = new WebCamTexture();
            webcam.Play();
            isActive = true;
        }
    }
    public Texture2D TakeAPicture()
    {
        Texture2D webCamImage = new Texture2D(webcam.width, webcam.height);
        webCamImage.SetPixels(webcam.GetPixels());
        webCamImage.Apply();

        return webCamImage;
    }

    public void TakePhotoToPreview(Renderer preview)
    {
        Texture2D image = TakeAPicture();
        preview.material.mainTexture = image;

        float ratio = (float)image.width / (float)image.height;
        Vector3 scale = preview.transform.localScale;
        scale.x = scale.y * ratio;
        preview.transform.localScale = scale;
    }

    public void InstanciatePhoto(GameObject go)
    {
        //colocar para instancias das imagens
    }
    #endregion
}
