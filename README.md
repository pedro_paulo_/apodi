## Dependências
1. Visual Studio 2017 (A versõa mais nova da ferramenta Visual Studio, sendo ela Visual studio 2019, possui problemas de compatibilidade com dependencias como: MRTK). Disponível em: https://visualstudio.microsoft.com/pt-br/vs/older-downloads/
2. Windows SDK 18362+ (Atual versão do projeto é 18362, porém possui retrocompatibilidade e até o momento não foi compartilhado nenhum problema com a versão). Disponível em: https://developer.microsoft.com/pt-br/windows/downloads/windows-10-sdk
3. Unity Hub (Instalador e gerenciador de versões para a plataforma Unity). Disponível em: https://unity3d.com/pt/get-unity/download
4. Unity Versão 2018.3.14f (Versões mais recentes podem ocasionamente possuir problemas nas builds). 

---

## Instalação

Foram feitas uma série de simplificações na instalação. E a versão atual necessita apenas das dependências e que os passos sejam seguidos.

1. Instação do Visual studio.
	1. Durante a instalação do Visual Studio 2017, deverão ser selecionadas as dependências para prover atendimento ao sistema dentre elas: Desenvolvimento para desktop com .NET, Desenvolvimento para desktop com C++, Desenvolvimento com a plataforma universal windows, Desenvolvimento de jogos com Unity. Opcionalmente pode-se escolher Desenvolvimento de jogos em C++.
	OBS: É indicado que a instalação do Visual Studio seja feita em um dispositivo SSD.

2. Instalação Unity Hub.
	1. Criar conta em: " https://store.unity.com ". Será necessário especificar a licença e para isso será necessário o login.
	2. Instalação do Unity Hub no link disponivél em dependências.
	3. Realizar o login na ferramenta Unity Hub.

3. Instalação da Unity
	1. Dirigir-se a aba de instalações (Installs). Nela será necessário adiocionar a versão desejada especifícada na dependência.
	2. Selecionar os módulos: UWP Build Support(IL2CPP), WebGL Build Support, Windows Build Support(IL2CPP), UWP Build Support(.NET). É opcional a marcação do módulo: Vuforia Augmented Reality Support.

O ambiente presente no Git já possui MRTK configurado, então não será necessária configuração.

Qualquer dúvida ou problema estarei disponível em: PedroPPMN@gmail.com